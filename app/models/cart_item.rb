class CartItem 
attr_reader :productid, :quantity

  def initialize productid, quantity=1
    @productid= productid
	@quantity = quantity
  end
  
  def product
    Product.find productid
  end
  
  def destroy productid
    @productid=nil
	@quantity=nil
  end
  
  def quantity_increment
    @quantity += 1
  end
  
  def quantity_decrement
    @quantity -=1
  end
    
  def total_cost
    product.unitcost.to_s.gsub(/[$,]/,'').to_f * quantity
  end
end