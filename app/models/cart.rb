class Cart 
attr_reader :products

QUANTITIES=[
    ["1", 1],
	["2", 2],
	["3", 3],
	["4", 4],
	["5", 5],
	["6", 6],
	["7", 7],
	["8", 8],
	["9", 9],
	["10", 10]
  ]

  def self.build_from_hash hash
    products= if hash["cart"] then
	  hash["cart"]["products"].map do |product|
	    CartItem.new product["productid"], product["quantity"]
	  end
	else
	  []
	end
	new products
  end
  
  def initialize products=[]
    @products= products
  end
  
  def add_product productid
    product = @products.find{ |product| product.productid == productid }
	if product
	  product.quantity_increment
	else
      @products << CartItem.new(productid)
    end
  end
  
  
  def is_empty?
    @products.empty?
  end
  
  def product_count
    @products.length
  end
  
  def serialize
    products = @products.map do |product| 
	{"productid" => product.productid, "quantity" => product.quantity}
	end
    {
	  "products" => products
	}
  end
  
  def total_cost
    @products.inject(0) { |sum, product| sum + product.total_cost }
  end
  
end
