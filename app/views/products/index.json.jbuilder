json.array!(@products) do |product|
  json.extract! product, :productid, :productname, :unitcost, :productimage
  json.url product_url(product, format: :json)
end
