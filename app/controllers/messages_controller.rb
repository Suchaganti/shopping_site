class MessagesController < ApplicationController
  def new
    @message = Message.new
  end

  def create
    @message = Message.new(params[:message])
    if @message.valid?
	  send_message
	  
      # TODO send message here
	  flash[:success]="Message delivered successfully! Thank you for contacting us."
      redirect_to contact_path
    else
      render "new"
    end
  end
  
  private
  
  def send_message
    MessageMailer.send_message_email(@message).deliver
  end
  
end
