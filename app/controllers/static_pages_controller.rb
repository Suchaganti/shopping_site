class StaticPagesController < ApplicationController

  def home
    @products= Product.where(:productid => [887,896,4678,2890,2818,5040,2149,11784,6426,10098])
	@productbests= OrderItem.joins(:product).group(:productid).select('productid,remoteproductimage, productname, unitcost,count(product_id) as count_col1').order('count_col1 DESC').limit(10)
  end

  def help
  end
  
  def contact
  end
end
