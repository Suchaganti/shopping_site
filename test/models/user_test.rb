require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @user = User.new(name: "Example User", email: "user@example.com", 
                     password: "example", password_confirmation: "example")
  end

  def user_valid 
    assert @user.valid?
  end
  
  def name_present 
    @user.name = "     "
    assert_not @user.valid?
  end
  
  def email_present 
    @user.email = "     "
    assert_not @user.valid?
  end
  
   def name_not_long
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  def email_not_long
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end
  
  def accept_valid_emailAddress
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  def reject_invalid_emailAddress
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  def email_unique
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
   def password_present
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  def password_min_length
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  def authenticated
    assert_not @user.authenticated?('')
  end
  
end
