# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151016044829) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "fullname"
    t.text     "address_line_1"
    t.text     "address_line_2"
    t.string   "city"
    t.string   "region"
    t.string   "country"
    t.string   "postal_code"
    t.string   "phone"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "addresses", ["user_id"], name: "index_addresses_on_user_id", using: :btree

  create_table "categories", primary_key: "categoryid", force: :cascade do |t|
    t.string "categoryname", limit: 50
  end

  add_index "categories", ["categoryname"], name: "categories_categoryname_key", unique: true, using: :btree

  create_table "order_items", force: :cascade do |t|
    t.integer "order_id"
    t.integer "product_id"
    t.integer "quantity"
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "address_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state",      default: "new"
    t.integer  "user_id"
  end

  add_index "orders", ["address_id"], name: "index_orders_on_address_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "products", primary_key: "productid", force: :cascade do |t|
    t.string  "productname",        limit: 5000
    t.integer "category_id"
    t.text    "productimage"
    t.text    "remoteproductimage"
    t.integer "upc",                                        default: "nextval('products_upc_seq'::regclass)", null: false
    t.money   "unitcost",                         scale: 2
    t.integer "quantity"
    t.string  "color",              limit: 5000
    t.string  "size",               limit: 5000
    t.string  "modelnumber",        limit: 5000
    t.string  "brand",              limit: 5000
    t.string  "department",         limit: 5000
    t.string  "reviews",            limit: 50000
  end

  add_index "products", ["productname"], name: "products_productname_key", unique: true, using: :btree
  add_index "products", ["upc"], name: "products_upc_key", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "remember_digest"
    t.boolean  "admin",             default: false
    t.string   "activation_digest"
    t.boolean  "activated"
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_foreign_key "addresses", "users"
  add_foreign_key "order_items", "orders", name: "order_items_order_id_fkey"
  add_foreign_key "order_items", "products", primary_key: "productid", name: "order_items_product_id_fkey"
  add_foreign_key "orders", "addresses"
  add_foreign_key "orders", "users"
  add_foreign_key "products", "categories", primary_key: "categoryid", name: "products_category_id_fkey"
end
