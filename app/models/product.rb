class Product < ActiveRecord::Base
  self.table_name="products"
  self.primary_key="productid"
  
  mount_uploader :productimage, ProductimageUploader
  belongs_to :category
  has_many :order_items
  validates_presence_of :productname,:unitcost
  
  SORT_VALUES=[
    [ "Price: High to Low", :desc],
	["Price: Low to High", :asc]
  ]
  
  
    
  def image_url
    productimage.present? ? productimage_url : remoteproductimage	
  end
  
  def self.search(search)
    if search
      where('lower(productname) LIKE ?', "%#{search.downcase}%")
    else
      all
    end
  end
end
