class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate_account
      log_in user
      flash[:success] = "Your Account is activated now!"
      redirect_to root_url
    else
      flash[:danger] = "Invalid activation link! Please check your link"
      redirect_to root_url
    end
  end
end
