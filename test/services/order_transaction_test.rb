require "test_helper"

class OrderTransactionTest < MiniTest::Test
  include FactoryGirl::Syntax::Methods
  
  def test_create_transaction
    order = Order.new
	order.order_items << OrderItem.new(product: build(:product), quantity: 1)
	nonce = Braintree::Test::Nonce::Transactable
    transaction = OrderTransaction.new order, nonce
	transaction.execute
	assert transaction.is_ok?, "Transaction Failed"
  end
end