class EditPasswordController < ApplicationController
  before_action :get_user
 
  def edit_userPassword
  end	
  
  def update_password
	if params[:user][:password].empty?
		flash.now[:danger]="Password can't be empty"
		render 'edit'
	elsif @user.update_attributes(user_params)
		log_in @user
		flash[:success]= "Password has been reset."
		redirect_to @user
	else
		render 'edit'
	end
  end
  
  private
  
  def user_params
	params.require(:user).permit(:password,:password_confirmation)
  end
  
 def get_user
      @user = User.find_by(params[:id])
    end

	
end
