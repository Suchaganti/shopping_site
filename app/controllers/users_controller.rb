class UsersController < ApplicationController
  before_action :sign_in_user, only: [:index,:edit,:update,:destroy, :show]
  before_action :validate_correct_user,   only: [:edit, :update]
  before_action :admin_user, only: [:destroy,:index,:show]
  def new
    @user = User.new
  end
  
  def index
	@users=User.paginate(page: params[:page])
  end
  
  def show
    begin
	  @user=User.find(params[:id])
	rescue ActiveRecord::RecordNotFound
	  flash[:warning] = "User Not Found"
	  redirect_to root_path 
	end
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      # Handle a successful save.
	  @user.send_activation_email
	  flash[:success] = "Welcome to Achats "+@user.name+" !!!. Please check your email to activate your account"
	  redirect_to root_path
    else
      render 'new'
    end
  end
  
  def edit
    @user=User.find(params[:id])
  end
    
  def update
   @user=User.find(params[:id])
	if @user.update_attributes(user_params)
		flash[:success]="Account information updated"
		redirect_to edit_user_path
	else
		render 'edit'
	end
  end
  
  def destroy
	User.find(params[:id]).destroy
	flash[:success]="User is deleted"
	redirect_to users_url
  end
  
  private

  
  def user_params
    params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
  end
  
  def sign_in_user
    unless logged_in?
	  store_location
      flash[:danger] = "Please Sign in."
      redirect_to login_url
    end
  end
  
  def validate_correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end
end
