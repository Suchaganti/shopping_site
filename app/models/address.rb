class Address < ActiveRecord::Base
  belongs_to :user
  has_many  :orders, dependent: :destroy
  
  def full_address
  <<EOF
#{address_line_1} #{address_line_2}
#{postal_code} #{city}
#{country}
EOF
  end
end
