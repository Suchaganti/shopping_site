class OrderForm
  include ActiveModel::Model
  attr_accessor :user, :order,:address
  attr_writer :cart
  
  def save
	  save_address
  end
  
  def has_errors?
    user.errors.any?
  end
  
  private
  
  def is_valid?
    user.valid?
  end
  
  def save_address
    address.save
	@order = Order.create! address: address, user_id: user.id
	build_order_products
  end
  
  def build_order_products
    @cart.products.each do |product|
      order.order_items.create! product_id: product.productid, quantity: product.quantity
	end
  end
  
  
end