class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :user, index: true, foreign_key: true
      t.string :fullname
      t.text :address_line_1
      t.text :address_line_2
      t.string :city
      t.string :region
      t.string :country
      t.string :postal_code
      t.string :phone

      t.timestamps null: false
    end
  end
end
