class Category < ActiveRecord::Base
  self.table_name="categories"
  self.primary_key="categoryid"
  has_many :products, dependent: :destroy
  
    SORT_VALUES=[
    [ "Price: High to Low", :desc],
	["Price: Low to High", :asc]
  ]
end
