class MessageMailer < ApplicationMailer
  default to: "suchaganti@mix.wvu.edu"
  def send_message_email message
    @message = message
    mail from: @message.email, subject: "Need Help"
  end
end
