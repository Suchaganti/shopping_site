class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  before_filter :get_categories, :get_products, :get_cart, :get_order
  attr_accessor :admin
  
  def get_categories
    @categories= Category.all
  end
  
  def get_products
    @products= Product.search(params[:search]).paginate(page: params[:page], per_page: 15)
  end
  
  def get_cart
     @cart = Cart.build_from_hash session
  end
  
  def get_order
    if(current_user)
      @orders= current_user.orders
	end
  end
  
  def home
  end
  
  def admin_user
     unless current_user.admin?
	  flash[:warning]= "You cannot perform this operation."
      redirect_to root_path
    end
  end
end