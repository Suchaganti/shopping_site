class OrdersController < ApplicationController
  before_action :sign_in_user, only: [:index]
  before_filter :get_cart
  before_action :admin_user, only: [ :index]
  
  def new
  end
  
  def create
    @order_form = OrderForm.new(
	  address: current_user.addresses.new(order_address_params[:address]),
	  user: current_user,
	  cart: @cart
	)
	if @order_form.save
	  notify_user
	  if payment_user
	    flash[:success]="Your Order is placed. Thank you for shopping with us!"
	    redirect_to root_path
		session["cart"]= nil
	  else
	    flash[:warning]="Something went wrong with your credit card. Please try another"
		redirect_to new_payment_order_path(@order_form.order)
	  end
	else
	  render "carts/checkout"
	end
  end
  
  def update
    @order = Order.find(params[:id])
	@previous_status = @order.state
	
	if @order.update(order_state_params)
	  notify_user_Order_status
	  flash[:success] = "Order is updated"
	  redirect_to orders_path
	end
	@client_token = Braintree::ClientToken.generate
  end
  
  def index
    @orders = Order.order(created_at: :desc).all
  end
  
  def order_history
    @orders= current_user.orders
  end
  
  def new_payment
    @order = Order.find params[:id]
	@client_token = Braintree::ClientToken.generate
  end
  
  def pay
    @order = Order.find params[:id]
	transaction = OrderTransaction.new @order, params[:payment_method_nonce]
	transaction.execute
	if transaction.is_ok?
	  flash[:success]="Your Order is placed. Thank you for shopping with us!"
	  redirect_to root_path
	else
	  render "orders/new_payment"
	end
  end
  
  
  private 
  
  def sign_in_user
    unless logged_in?
	  store_location
      flash[:danger] = "Please Sign in."
      redirect_to login_url
    end
  end
 
  def order_address_params
    params.require(:order_form).permit(address: [:user_id,:fullname,:address_line_1,:address_line_2,:city,:region,:postal_code,:country,:phone])
  end  
  
  def notify_user
    OrderMailer.order_confirmation_email(@order_form.order,current_user).deliver
  end
  
  def notify_user_Order_status
    OrderMailer.status_changed(@order,@previous_status,User.find(@order.user_id)).deliver
  end
  
  def payment_user
    @order = @order_form.order
    transaction = OrderTransaction.new @order, params[:payment_method_nonce]
	transaction.execute
	transaction.is_ok?
  end
  
  def order_state_params
    params.require(:order).permit(:state)
  end
end	
