class CartsController < ApplicationController
  before_filter :get_cart
  before_action :sign_in_user, only: [:checkout]
  
  def add
    @cart.add_product params[:productid]
	session["cart"] = @cart.serialize
	product= Product.find(params[:productid])
    flash[:success] = "Added #{product.productname} to cart"
    redirect_to :back
  end
  
   def destroy
    session["cart"]= Hash.new(0)
	session["cart"] = @cart.serialize
	session["cart"]["products"].delete_if {|hash| hash["productid"] == params[:productid]  } 
	product= Product.find(params[:productid])
    flash[:success] = "Removed #{product.productname} from cart"
    redirect_to cart_path	
  end
  
  def show
  end
  
  def update
    product.unitcost.to_s.gsub(/[$,]/,'').to_f * quantity
	redirect_to carts_path
  end
  
  def checkout
    @order_form= OrderForm.new user: User.new
	@client_token = Braintree::ClientToken.generate
  end
  
  private 
  
  def sign_in_user
    unless logged_in?
	  store_location
      flash[:danger] = "Please Sign in."
      redirect_to login_url
    end
  end
end