class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_filter :get_cart
  before_action :admin_user, only: [ :new, :edit, :update, :create, :destroy ]
  autocomplete :product, :productname
  
  
  def new
  	@product= Product.new
  end
  
  def index
    if params[:sort_value]
  	  @products= Product.search(params[:search]).order('unitcost ' + params[:sort_value]).paginate(page: params[:page], per_page: 24)
    else
	  @products= Product.search(params[:search]).paginate(page: params[:page], per_page: 24)
	end  
  end
  
  def show
  end
  
  def edit
  end
  
  def create
    @product = Product.new(product_params)
    respond_to do |format|
      if @product.save
	    flash[:success] = "Product was successfully created."
        format.html { redirect_to @product}
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end
  
   def update
    respond_to do |format|
      if @product.update(product_params)
	    flash[:success] = "You have successfully updated the product."
        format.html { redirect_to @product}
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @product.destroy
    respond_to do |format|
	  flash[:success] = 'Product is deleted' 
      format.html { redirect_to products_url }
      format.json { head :no_content }
    end
  end
  
   private
    def set_product
      @product = Product.find(params[:productid])
    end
	

	def product_params
      params.require(:product).permit(:productname, :unitcost, :productimage,:remoteproductimage, :category_id)
    end
end
