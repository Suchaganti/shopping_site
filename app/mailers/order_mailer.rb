class OrderMailer < ApplicationMailer
  default from: "suchaganti@mix.wvu.edu"
  def order_confirmation_email order,user
    @order = order
	@user=user
    mail to: user.email, subject: "Your Achats order (##{order.id})"
  end
  
  def status_changed order, previous_status,user
    @order = order
	@user=user
    @previous_status = previous_status
	mail to: user.email, subject: "Your Achats order (##{order.id}) is #{order.state}"
  end
end
