require "test_helper"

class CartTest < ActiveSupport::TestCase
  def test_adds_product
    cart= Cart.new
	cart.add_product 1
	assert_equal cart.is_empty?, false
  end
  
  def test_adds_quantity
    cart= Cart.new
	3.times{ cart.add_product 1}
	
	assert_equal 1, cart.products.length
	assert_equal 3, cart.products.first.quantity
  end
  
  def test_knows_products
    product = Product.create! productname: "Two States", unitcost: "$15"
	cart = Cart.new
	cart.add_product product.productid
	
	assert_kind_of Product, cart.products.first.product
  end
  
  def test_cart_serializes_to_hash
    cart = Cart.new
	cart.add_product 1
	
	assert_equal cart.serialize, cart_session_hash["cart"]
  end
  
  def test_cart_builds_from_hash
    cart = Cart.build_from_hash cart_session_hash
	assert_equal 1, cart.products.first.productid
  end
  
  private
  
  def cart_session_hash
    {
	  "cart" => {
	    "products" => [
		  {"productid" => 1, "quantity" => 1}
		]
	  }
	}
  end
end