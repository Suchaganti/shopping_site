class OrderTransaction
  def initialize order, nonce
    @order = order
	@nonce = nonce
  end
  
  def execute
    @status = Braintree::Transaction.sale(
	  amount: order.total_amount,
	  payment_method_nonce: nonce
	)
  end
  
  def is_ok?
    @status.success?
  end
  
  private
  attr_reader :order, :nonce
end