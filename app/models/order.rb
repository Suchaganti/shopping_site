class Order < ActiveRecord::Base
  belongs_to :address
  belongs_to  :user
  has_many :order_items, dependent: :destroy
  
  def total_amount
    order_items.inject(0) { |sum, product| sum + product.total_amount }
  end
  
  STATES=[
    [ "New", :new],
	["Dispatched", :dispatched],
	["Delivered", :delivered],
	["Cancelled", :cancelled]
  ]
end
