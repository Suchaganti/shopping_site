require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @user = users(:john)
  end
  
  def test_unsuccessful_edit
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: { name:  "",
                                    email: "john@invalid",
                                    password:              "john",
                                    password_confirmation: "jack" }
    assert_template 'users/edit'
  end
  
  def test_successful_edit
    get edit_user_path(@user)
	log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    name  = "John"
    email = "smith@example.com"
    patch user_path(@user), user: { name:  name,
                                    email: email,
                                    password:              "",
                                    password_confirmation: "" }
    assert_not flash.empty?
    assert_redirected_to edit_user_path(@user)
    @user.reload
    assert_equal name,  @user.name
    assert_equal email, @user.email
  end
end
