class AddressesController < ApplicationController

def new
    @user = User.new
  end
  
private
  
  def user_address_params
    params.require(:address).permit(:fullname)
  end
end
