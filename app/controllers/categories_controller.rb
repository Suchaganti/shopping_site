class CategoriesController < ApplicationController
before_action :set_category, only: [:show, :edit, :update, :destroy]
before_action :admin_user, only: [ :new, :edit, :update, :create, :destroy, :index ]
  def new
    @category= Category.new
  end
  
  def edit
  end
  
  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
	    flash[:success] = 'Category was successfully created.'
        format.html { redirect_to categories_url }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    respond_to do |format|
      if @category.update(category_params)
	    flash[:success]= 'Category is updated' 
        format.html { redirect_to categories_url }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def index
    @categories = Category.all # getting all the users!
  end
  
  def show
    if params[:sort_value]
      @products = @category.products.search(params[:search]).order('unitcost '+params[:sort_value]).paginate(page: params[:page], per_page: 24)
    else
	  @products = @category.products.search(params[:search]).paginate(page: params[:page], per_page: 24)
	end
  end
  
  def destroy
    @category.destroy
    respond_to do |format|
	  flash[:success] = 'Category was successfully destroyed.' 
      format.html { redirect_to categories_url }
      format.json { head :no_content }
    end
  end
  
  private
    def set_category
	  begin
        @category = Category.includes(:products).find(params[:categoryid])
	  rescue ActiveRecord::RecordNotFound
	    flash[:warning] = "Category Not Found"
	    redirect_to root_path 
	  end
    end
	
    def category_params
      params.require(:category).permit(:categoryname)
    end
end
