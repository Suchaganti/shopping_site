require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  def setup
    @user = users(:john)
	@other_user = users(:james)
  end
  
  def test_should_get_new
    get :new
    assert_response :success
  end

   def test_redirect_edit_not_loggedin
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  def test_redirect_update_not_loggedin
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  def test_redirect_edit_loggedin_wrong_user
    log_in_as(@other_user)
    get :edit, id: @user
    assert flash.empty?
    assert_redirected_to root_url
  end

  def test_redirect_update_loggedin_wrong_user
    log_in_as(@other_user)
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert flash.empty?
    assert_redirected_to root_url
  end
  
end
