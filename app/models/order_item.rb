class OrderItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :product
  
  def total_amount
    self.quantity * self.product.unitcost.to_s.gsub(/[$,]/,'').to_f
  end
  
end
